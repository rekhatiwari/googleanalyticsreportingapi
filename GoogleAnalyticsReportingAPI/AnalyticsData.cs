﻿using System;

namespace GoogleAnalyticsReportingAPI
{
    public class AnalyticsData
    {

        //ga:date,ga:campaign,ga:pagePathLevel2
        //ga:adsenseRevenue,ga:adsensePageImpressions,ga:adsenseAdsClicks,ga:adsenseCTR,ga:adsenseECPM,ga:pageviewsPerSession,ga:pageviews,ga:sessions,ga:bounceRate
        //ga:sessionDuration

        public DateTime Date { get; set; }
        public string GaId { get; set; }
        public string ViewName { get; set; }
        public string DomainName { get; set; }
        public string Source { get; set; }
        public string Campaign { get; set; }
        public string AdContent { get; set; }
        public string Keyword { get; set; }
        public string DeviceCategory { get; set; }
        public string CountryCode { get; set; }
        public decimal AdsenseRevenue { get; set; }
        public int AdsensePageImpressions { get; set; }
        public int AdsenseAdsClicks { get; set; }
        public decimal AdsenseCTR { get; set; }
        public decimal AdsenseECPM { get; set; }
        public int PageViewsPerSession { get; set; }
        public int PageViews { get; set; }
        public int Sessions { get; set; }
        public decimal BounceRate { get; set; }
        public int SessionDuration { get; set; }

    }
}
