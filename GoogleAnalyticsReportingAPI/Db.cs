﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace GoogleAnalyticsReportingAPI
{
    public class Db
    {

        public static void LoadAnalyticsData(List<AnalyticsData> dataList)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["RSTConnectionString"].ConnectionString))
            {
                con.Open();

                // Delete stale data
                var date = dataList.Select(d => d.Date).First();
                var gaId = dataList.Select(d => d.GaId).First();
                using (var cmd = con.CreateCommand())
                {
                    cmd.CommandText = "DELETE FROM RST.dbo.AnalyticsData WHERE [Date] = @date AND GaId = @gaid";
                    cmd.Parameters.AddWithValue("@date", date);
                    cmd.Parameters.AddWithValue("@gaid", gaId);
                    cmd.ExecuteNonQuery();
                }


                // Refresh data
                using (SqlTransaction tran = con.BeginTransaction())
                {
                    SqlBulkCopy bc = new SqlBulkCopy(con,
                         SqlBulkCopyOptions.CheckConstraints |
                         SqlBulkCopyOptions.FireTriggers |
                         SqlBulkCopyOptions.KeepNulls, tran);

                    bc.BatchSize = 100;
                    bc.DestinationTableName = "AnalyticsData";

                    bc.ColumnMappings.Clear();
                    bc.ColumnMappings.Add("Date", "Date");
                    bc.ColumnMappings.Add("GaId", "GaId");
                    bc.ColumnMappings.Add("ViewName", "ViewName");
                    bc.ColumnMappings.Add("DomainName", "DomainName");
                    bc.ColumnMappings.Add("Source", "Source");
                    bc.ColumnMappings.Add("Campaign", "Campaign");
                    bc.ColumnMappings.Add("AdContent", "AdContent");
                    bc.ColumnMappings.Add("Keyword", "Keyword");
                    bc.ColumnMappings.Add("DeviceCategory", "DeviceCategory");
                    bc.ColumnMappings.Add("CountryCode", "CountryCode");
                    bc.ColumnMappings.Add("AdsenseRevenue", "AdsenseRevenue");
                    bc.ColumnMappings.Add("AdsensePageImpressions", "AdsensePageImpressions");
                    bc.ColumnMappings.Add("AdsenseAdsClicks", "AdsenseAdsClicks");
                    bc.ColumnMappings.Add("AdsenseCTR", "AdsenseCTR");
                    bc.ColumnMappings.Add("AdsenseECPM", "AdsenseECPM");
                    bc.ColumnMappings.Add("PageViewsPerSession", "PageViewsPerSession");
                    bc.ColumnMappings.Add("PageViews", "PageViews");
                    bc.ColumnMappings.Add("Sessions", "Sessions");
                    bc.ColumnMappings.Add("BounceRate", "BounceRate");
                    bc.ColumnMappings.Add("SessionDuration", "SessionDuration");

                    bc.WriteToServer(dataList.AsDataReader());
                    tran.Commit();
                }
            }
        }

        public static void SCM_SPS_Analytics_Data_Populate()
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["RSTConnectionString"].ConnectionString))
            {
                con.Open();

                using (var command = new SqlCommand("SCM_SPS_AnalyticsData_Populate", con))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.ExecuteNonQuery();
                }
            }
        }

        public static void Populate_Ghost_Queue()
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["RSTConnectionString"].ConnectionString))
            {
                con.Open();

                using (var command = new SqlCommand("PopulateGhostQueue", con))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.ExecuteNonQuery();
                }
            }
        }

    }
}
