﻿namespace GoogleAnalyticsReportingAPI
{
    public class GAView
    {

        public string Id { get; set; }
        public string Name { get; set; }
        public string UnderlyingDomain { get; set; }

    }
}
