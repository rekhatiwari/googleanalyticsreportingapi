﻿using Google.Apis.Analytics.v3;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Security.Cryptography.X509Certificates;

namespace GoogleAnalyticsReportingAPI
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Running ...");

            var serviceAccountEmail = "690797082486-p131j03pjc4lkj573ibucqe265k10dm9@developer.gserviceaccount.com";
            var file = ConfigurationManager.AppSettings["X509KeyFilePath"];

            var certificate = new X509Certificate2(file, "notasecret", X509KeyStorageFlags.Exportable);

            ServiceAccountCredential credential = new ServiceAccountCredential(
               new ServiceAccountCredential.Initializer(serviceAccountEmail)
               {
                   Scopes = new[] { AnalyticsService.Scope.AnalyticsReadonly }
               }.FromCertificate(certificate));

            AnalyticsService service = new AnalyticsService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = "Qool Media - Analytics Puller"
            });

            Console.WriteLine("Client initialized ...");

            var metrics = "ga:adsenseRevenue,ga:adsensePageImpressions,ga:adsenseAdsClicks,ga:adsenseCTR,ga:adsenseECPM,ga:pageviewsPerSession,ga:pageviews,ga:sessions,ga:bounceRate";    //ga:sessionDuration (not playing nice with ga:adsenseRevenue, need to do on seperate pull)
            var metrics1 = "ga:sessionDuration,ga:sessions,ga:bounceRate,ga:pageviewsPerSession";
            var dimensions = "ga:date,ga:source,ga:campaign,ga:adContent,ga:keyword,ga:deviceCategory,ga:countryIsoCode"; //ga:date,ga:campaign

            // Define accounts/views to pull
            var views = new List<GAView>();
            //views.Add(new GAView() { Id = "ga:97695012", Name = "TravelVersed.co", UnderlyingDomain = "travelversed.co" });
           // views.Add(new GAView() { Id = "ga:100563026", Name = "TravelVersed - PST", UnderlyingDomain = "travelversed.co" });
           // views.Add(new GAView() { Id = "ga:107203510", Name = "FilthyRich - EST (with adsense)", UnderlyingDomain = "filthyrich.es" });
           // views.Add(new GAView() { Id = "ga:108428421", Name = "AutoVersed.com (EST)", UnderlyingDomain = "autoversed.com" });
          //  views.Add(new GAView() { Id = "ga:107656221", Name = "HealthVersed.com (EST)", UnderlyingDomain = "healthversed.com" });
           // views.Add(new GAView() { Id = "ga:116964765", Name = "CMs - WrestleNewz - EST", UnderlyingDomain = "wrestlenewz.com" });
            views.Add(new GAView() { Id = "ga:97379805", Name = "TodaysInfo.net (PST)", UnderlyingDomain = "todaysinfo.net" });
           // views.Add(new GAView() { Id = "ga:124487364", Name = "SmartFind.com - PST", UnderlyingDomain = "smartfind.com" });

            foreach (var view in views)
            {
                try
                {
                    Console.WriteLine("Requesting data for: {0} ...", view.Name);

                    Google.Apis.Analytics.v3.DataResource.GaResource.GetRequest request;
                    Google.Apis.Analytics.v3.DataResource.GaResource.GetRequest request1;
                    if (args.Length == 0)
                    {
                        request = service.Data.Ga.Get(view.Id, "today", "today", metrics);
                        request1 = service.Data.Ga.Get(view.Id, "today", "today", metrics1);
                    }
                    else
                    {
                        request = service.Data.Ga.Get(view.Id, "yesterday", "yesterday", metrics);
                        request1 = service.Data.Ga.Get(view.Id, "yesterday", "yesterday", metrics1);
                    }

                    request.Dimensions = dimensions;
                    request.SamplingLevel = DataResource.GaResource.GetRequest.SamplingLevelEnum.HIGHERPRECISION;   // https://developers.google.com/analytics/devguides/reporting/core/v3/reference#samplingLevel
                    request.MaxResults = 10000; // https://developers.google.com/analytics/devguides/reporting/core/v3/reference#maxResults

                    var result = request.Execute();

                    //making seperate request to fetch session duration
                    request1.Dimensions = dimensions;
                    request1.SamplingLevel = DataResource.GaResource.GetRequest.SamplingLevelEnum.HIGHERPRECISION;
                    request1.MaxResults = 10000;

                    var result1 = request1.Execute();

                    List<AnalyticsData> data = new List<AnalyticsData>();
                    if (result != null && result.Rows.Count > 0)
                    {
                        var i = 0;
                        foreach (var row in result.Rows)
                        {
                            int sessionDuration = 0;
                            try
                            {
                                sessionDuration = (result1 != null && result1.Rows.Count > 0) ? Convert.ToInt32(result1.Rows[i][7].ParseAs<decimal>()) : 0;
                            }
                            catch(Exception err)
                            {
                                sessionDuration = 0;
                            }
                            var ad = new AnalyticsData()
                            {
                                Date = DateTime.ParseExact(row[0], "yyyyMMdd", CultureInfo.InvariantCulture),
                                GaId = view.Id,
                                ViewName = view.Name,
                                DomainName = view.UnderlyingDomain,
                                Source = row[1].Truncate(50),
                                Campaign = row[2].Truncate(255),
                                AdContent = row[3].Truncate(255),
                                Keyword = row[4].Truncate(255),
                                DeviceCategory = row[5].Truncate(10),
                                CountryCode = row[6].Truncate(2),
                                AdsenseRevenue = row[7].ParseAs<decimal>(0),
                                AdsensePageImpressions = row[8].ParseAs<int>(0),
                                AdsenseAdsClicks = row[9].ParseAs<int>(0),
                                AdsenseCTR = row[10].ParseAs<decimal>(0),
                                AdsenseECPM = row[11].ParseAs<decimal>(0),
                                PageViewsPerSession = Convert.ToInt32(row[12].ParseAs<decimal>(0)),
                                PageViews = row[13].ParseAs<int>(0),
                                Sessions = row[14].ParseAs<int>(0),
                                BounceRate = row[15].ParseAs<decimal>(0),
                                SessionDuration = sessionDuration
                            };
                            data.Add(ad);
                            i++;
                        }
                    }

                    Console.WriteLine("Loading to db ...");
                    Db.LoadAnalyticsData(data);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("***ERROR: ", ex.Message);
                }

            }

            if (args.Length > 0)
            {
                try
                {
                    //Get Analytics data just populated to copied into SCM_SPS table as core data for Yesterday
                    Db.SCM_SPS_Analytics_Data_Populate();

                    //Refresh Ghost Queue entries for new Spend Accounts
                    Db.Populate_Ghost_Queue();
                }
                catch (Exception er)
                {
                    Console.WriteLine("***ERROR: ", er.Message);
                }
            }

            Console.WriteLine("Finished, goodbye world ...");
            //Console.WriteLine("Press <any> key to exit ...");
            //Console.ReadKey();
        }

    }
}
