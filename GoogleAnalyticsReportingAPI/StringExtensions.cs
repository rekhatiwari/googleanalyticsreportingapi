﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text.RegularExpressions;

namespace GoogleAnalyticsReportingAPI
{
    public static class StringExtensions
    {

        public static T ParseAs<T>(this string value) where T : struct
        {
            return value.ParseAs<T>(default(T));
        }
        public static T ParseAs<T>(this string value, T defaultValue) where T : struct
        {
            T? __value = value.TryParseAs<T>();
            return __value.HasValue ? __value.Value : defaultValue;

        }
        public static T? TryParseAs<T>(this string value) where T : struct
        {
            T __value = default(T);

            if (value == null) return null;

            // If is bool convert value to true/false
            if (typeof(T).Equals(typeof(bool)))
            {
                if (value.Equals("T", StringComparison.InvariantCultureIgnoreCase)) value = "true";
                else if (value.Equals("F", StringComparison.InvariantCultureIgnoreCase)) value = "false";
                else if (value.Equals("1", StringComparison.InvariantCultureIgnoreCase)) value = "true";
                else if (value.Equals("0", StringComparison.InvariantCultureIgnoreCase)) value = "false";
            }

            // Handle an Enum
            if (typeof(T).IsEnum)
            {
                if (value == null) return null;
                return new T?((T)Enum.Parse(typeof(T), value));
            }

            // Use the TryParse method
            MethodInfo __method = (MethodInfo)typeof(T).GetMethod("TryParse", new Type[] { typeof(string), typeof(T).MakeByRefType() });

            if (__method == null)
                throw new InvalidOperationException("Type '" + typeof(T).Name + "' does not contain a TryParse method.");

            object[] __args = new object[] { value, __value };
            if (!(bool)__method.Invoke(null, __args))
                return null;

            return new T?((T)__args[1]);
        }

        public static Uri ParseAsUri(this string value)
        {
            if (string.IsNullOrEmpty(value)) return null;

            return new Uri(value);
        }

        public static string Truncate(this string value, int maxLength)
        {
            if (value != null && value.Length > maxLength)
                value = value.Substring(0, maxLength);

            return value;
        }

        public static string StripHtmlTags(this string str)
        {
            if (string.IsNullOrEmpty(str)) return str;

            return Regex.Replace(str, "<[^>]*>", string.Empty);
        }

        public static string ParseFirstFourDigits(this string str)
        {
            if (string.IsNullOrEmpty(str)) return "";

            Regex regex = new Regex(@"(?<d>^\d{4,5})", RegexOptions.Compiled);
            Match m = regex.Match(str);
            if (m.Success)
            {
                return m.Groups[0].Value;
            }
            else
                return str;
        }

        public static List<T> SplitAs<T>(this string str) where T : struct
        {
            return str.SplitAs<T>(new char[] { ',' });
        }
        public static List<T> SplitAs<T>(this string str, char[] separator) where T : struct
        {
            List<T> __list = new List<T>();

            if (!string.IsNullOrEmpty(str))
            {
                foreach (var item in str.Split(separator))
                {
                    __list.Add(item.ParseAs<T>());
                }
            }

            return __list;
        }

        public static string Join(this IEnumerable<string> source, string separator)
        {
            return string.Join(separator, source);
        }

    }
}
